<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Strategis-Beranda</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
	
		<link href="<?php echo base_url('asset/css/bootstrap.min.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('asset/font-awesome/css/font-awesome.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('asset/css/plugins/morris/morris-0.4.3.min.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('asset/css/plugins/timeline/timeline.css');?>" rel="stylesheet">
            
		<script src="<?php echo base_url('asset/js/jquery.js');?>"></script>
		<script src="<?php echo base_url('asset/js/bootstrap.js');?>"></script>
		<script src="<?php echo base_url('asset/js/tinymce/tinymce.min.js');?>"></script>
		<script>
			tinymce.init({selector:'textarea'});
		</script>
	</head>
	<body>
		<center><img src="<?php echo base_url('asset/img/fti.png');?>" height="100%" width="100%"></center>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-left">
						<li class="btn"><a href="<?php echo base_url('responden/c_strategis');?>"><span class="text"><i class="fa fa-home"></i>&nbsp;Home</span></a></li>
						<li class="btn"><a href="<?php echo base_url('responden/c_isistrategis');?>"><span class="text"><i class="fa fa-pencil"></i>&nbsp;Isi Kuesioner</span></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="btn"><a href="<?php echo base_url('c_login/logout');?>"><span class="text"><i class="fa fa-fw fa-power-off"></i>&nbsp;Logout</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="content">
			<center><div class="jumbotron">
				<h3>Selamat Datang <?php echo $this->session->userdata['username'];?></h3><br>
				<h3>Terimakasih Anda telah ikut berpartisipasi mengisi kuesioner ini</h3>
			</div></center>
		</div>
		
		<!-- js placed at the end of the document so the pages load faster -->
		<script src="<?php echo base_url()?>asset/js/bootstrap.js"></script>
		<script src="<?php echo base_url()?>asset/js/jquery.js"></script>
	</body>
</html>