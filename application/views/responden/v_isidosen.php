<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Dosen-IsiKuesioner</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
	
		<link href="<?php echo base_url('asset/css/bootstrap.min.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('asset/font-awesome/css/font-awesome.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('asset/css/plugins/morris/morris-0.4.3.min.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('asset/css/plugins/timeline/timeline.css');?>" rel="stylesheet">
			
		<script src="<?php echo base_url('asset/js/jquery.js');?>"></script>
		<script src="<?php echo base_url('asset/js/bootstrap.js');?>"></script>
		<script src="<?php echo base_url('asset/js/tinymce/tinymce.min.js');?>"></script>
		<script>
			tinymce.init({selector:'textarea'});
		</script>
	</head>
	<body>
		<center><img src="<?php echo base_url('asset/img/fti.png');?>" height="100%" width="100%"></center>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-left">
						<li class="btn"><a href="<?php echo base_url('responden/c_dosen');?>"><span class="text"><i class="fa fa-home"></i>&nbsp;Home</span></a></li>
						<li class="btn"><a href="<?php echo base_url('responden/c_isidosen');?>"><span class="text"><i class="fa fa-pencil"></i>&nbsp;Isi Kuesioner</span></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="btn"><a href="<?php echo base_url('c_login/logout');?>"><span class="text"><i class="fa fa-fw fa-power-off"></i>&nbsp;Logout</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="jumbotron col-sm-12">
				<div class="widget-title" align="center">
					<h4><b>Isi Kuesioner</b></h4>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="modal-body">
							<form method="post" action="<?php echo site_url('responden/c_isidosen/aksi_tambahisi'); ?>" id="myForm" name="myForm">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<td colspan="9">
											<b><center>Pilihan Jawaban</center></b>
											<ol>
												<!--<li>Amat Sangat Tidak Mengetahui</li>-->
												<li>Sangat Tidak Setuju</li>
												<li>Tidak Setuju</li>
												<li>Cukup</li>
												<li>Setuju</li>
												<li>Sangat Setuju</li>
											</ol>			
											</td>
										</tr>
										<tr align="center">
											<td><b>No</b></td>
											<td><b>Kode Pertanyaan</b></td>
											<td><b>Pertanyaan</b></td>
											<td><b>1</b></td>
											<td><b>2</b></td>
											<td><b>3</b></td>
											<td><b>4</b></td>
											<td><b>5</b></td>
											<!--<td><b>6</b></td>-->
										</tr>
									</thead>
									<tbody>
									<?php
									$no=1;
									$i=1;
									foreach ($tampil as $u){
			
										echo '<tr>';
											echo '<td>'.$no++.'</td>';
											echo '<td> <input type="text" name="kodepertanyaan'.$i.'" value="'.$u->kodepertanyaan.'" readonly=readonly/></td>';
											echo '<td>'.$u->pertanyaan.'</td>';
											echo '<td> <input type="radio" name="jawaban'.$i.'" value="1" required></td>';
											echo '<td> <input type="radio" name="jawaban'.$i.'" value="2" required></td>';
											echo '<td> <input type="radio" name="jawaban'.$i.'" value="3" required></td>';
											echo '<td> <input type="radio" name="jawaban'.$i.'" value="4" required></td>';
											echo '<td> <input type="radio" name="jawaban'.$i.'" value="5" required></td>';
											//echo '<td> <input type="radio" name="jawaban'.$i.'" value="6" required></td>';			
										echo '</tr>';
									$i++;
									?>
									<?php
									}
									?>	
									</tbody>
								</table>
								<label>Kritik dan saran :</label>
								<textarea class="form-control" rows="5"  style="width: 685px; height: 85px;"></textarea>
								</div>
								<input type="submit" class="btn btn-sm btn-primary"  value="Simpan">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- js placed at the end of the document so the pages load faster -->
		<script src="<?php echo base_url()?>asset/js/bootstrap.js"></script>
		<script src="<?php echo base_url()?>asset/js/jquery.js"></script>
	</body>
</html>