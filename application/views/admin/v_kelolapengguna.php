<!DOCTYPE html>
<html lang="en">
  <head>
	<title>Auditor-KelolaUser</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	
	
	<link href="<?php echo base_url('asset/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/font-awesome/css/font-awesome.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/plugins/morris/morris-0.4.3.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/plugins/timeline/timeline.css');?>" rel="stylesheet">
        
            
    <script src="<?php echo base_url('asset/js/jquery.js');?>"></script>
    <script src="<?php echo base_url('asset/js/bootstrap.js');?>"></script>
    <script src="<?php echo base_url('asset/js/tinymce/tinymce.min.js');?>"></script>
    <script>
        tinymce.init({selector:'textarea'});
    </script>
  </head>

  <body>
	<center><img src="<?php echo base_url('asset/img/fti.png');?>" height="100%" width="100%"></center>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-collapse collapse">
	            <ul style="font-color:black;" class="nav navbar-nav navbar-left">
					<li class="btn"><a href="<?php echo base_url('admin/c_auditor');?>"><span class="text"><i class="fa fa-home"></i>&nbsp;Home</span></a></li>
	                <li class="btn"><a href="<?php echo base_url('admin/c_kelolapengguna');?>"><span class="text"><i class="fa fa-user"></i>&nbsp;Kelola Pengguna</span></a></li>
	                <li class="btn"><a href="<?php echo base_url('admin/c_kelolakuesioner');?>"><span class="text"><i class="fa fa-fw fa-edit"></i>&nbsp;Kelola Kuesioner</span></a></li>
	                <li class="btn"><a href="<?php echo base_url('admin/c_rekapitulasi');?>"><span class="text"><i class="fa fa-fw fa-bar-chart-o"></i>&nbsp;Rekapitulasi</span></a></li>
	            </ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="btn"><a href="<?php echo base_url('c_login/logout');?>"><span class="text"><i class="fa fa-fw fa-power-off"></i>&nbsp;Logout</span></a></li>
	            </ul>
	        </div>
		</div>
	</nav>
	
	<div class="widget-title" align="center">
		<b><h3>Daftar Pengguna</h5></b>
	</div></br>
	<div class="widget-content nopadding" align="center">
		<a href="<?php echo site_url('admin/c_kelolapengguna/tambahpengguna');?>" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Tambah Pengguna</a><br></br>
	</div>
	<div class="container">
		<div class="jumbotron">
			<table class="table table-bordered table-striped">
				<thead>
					<tr align="center">
						<td><b>No</b></td>
						<td><b>Kode Pengguna</b></td>
						<td><b>Nim/Kode Dosen</b></td>
						<td><b>Nama</b></td>
						<td><b>Level</b></td>
						<td><b>Aksi</b></td>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 1;
			
					foreach ($user as $u){
					?>
					<tr>
						<td><?php echo $no;?></td>
						<td><?php echo $u->kodepengguna;?></td>
						<td><?php echo $u->username;?></td>
						<td><?php echo $u->nama;?></td>
						<td><?php echo $u->level;?></td>
						<td>
							<a href="<?php echo site_url('admin/c_kelolapengguna/ubahpengguna/'.$u->kodepengguna);?>" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
							<a href="<?php echo site_url('admin/c_kelolapengguna/hapuspengguna/'.$u->kodepengguna);?>" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin ingin menghapus data ini?')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
						</td>
					</tr>
					<?php
					$no++;
					}
					?>
				</tbody>
			</table>
			<div>
				<?php
					//echo $this->pagination->create_link();
				?>
			</div>
		</div>
	</div>
	
<!-- js placed at the end of the document so the pages load faster -->
	
	<script src="<?php echo base_url()?>asset/datatable/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>asset/js/bootstrap.js"></script>
    <script src="<?php echo base_url()?>asset/js/jquery.js"></script>
  </body>
</html>