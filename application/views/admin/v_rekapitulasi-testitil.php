<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Auditor-Rekapitulasi</title>
	
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
	
		<link href="<?php echo base_url('asset/css/bootstrap.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('asset/css/bootstrap.min.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('asset/font-awesome/css/font-awesome.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('asset/csss/plugins/morris.css');?>" rel="stylesheet">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/login.css" rel="stylesheet">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/freelancer.css" rel="stylesheet">
            
		<script src="<?php echo base_url('asset/js/jquery.js');?>"></script>
		<script src="<?php echo base_url('asset/js/bootstrap.js');?>"></script>
		<script src="<?php echo base_url('asset/js/tinymce/tinymce.min.js');?>"></script>
		<script>
			tinymce.init({selector:'textarea'});
		</script>
	</head>
	
	<body>
		<center><img src="<?php echo base_url('asset/img/fti.png');?>" height="100%" width="100%"></center>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-collapse collapse">
					<ul style="font-color:black;" class="nav navbar-nav navbar-left">
						<li class="btn"><a href="<?php echo base_url('admin/c_auditor');?>"><span class="text"><i class="fa fa-home"></i>&nbsp;Home</span></a></li>
						<li class="btn"><a href="<?php echo base_url('admin/c_kelolapengguna');?>"><span class="text"><i class="fa fa-user"></i>&nbsp;Kelola Pengguna</span></a></li>
						<li class="btn"><a href="<?php echo base_url('admin/c_kelolakuesioner');?>"><span class="text"><i class="fa fa-fw fa-edit"></i>&nbsp;Kelola Kuesioner</span></a></li>
						<li class="btn"><a href="<?php echo base_url('admin/c_rekapitulasi');?>"><span class="text"><i class="fa fa-fw fa-bar-chart-o"></i>&nbsp;Rekapitulasi</span></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="btn"><a href="<?php echo base_url('c_login/logout');?>"><span class="text"><i class="fa fa-fw fa-power-off"></i>&nbsp;Logout</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12">
					<center><h4><b>Hasil Rekapitulasi Keseluruhan</b></h4></center><br><br>
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>1</th>
										<th>Jumlah Responden : <?php echo ($tampil/54);?></th>
									</tr>
									<tr>
										<th>2</th>
										<th>Jumlah Pertanyaan : <?php echo $jumlahpertanyaan;?></th>
									</tr>
										<tr>
										<th>3</th>
										<th>Jumlah Jawaban
											<ol>
												<li>Jawaban 1 : <?php echo $tampilsatu;?></li>
												<li>Jawaban 2 : <?php echo $tampildua;?></li>
												<li>Jawaban 3 : <?php echo $tampiltiga;?></li>
												<li>Jawaban 4 : <?php echo $tampilempat;?></li>
												<li>Jawaban 5 : <?php echo $tampillima;?></li>
												<!--<li>Jawaban 6 : <?php// echo $tampilenam;?></li>-->
											</ol>
										</th>
									</tr>
									<tr>
									<?php //memindahkan lvl 0 to lvl 1 cont*
										// *test langsung tidak dikalikan terlebih dahulu*
										/*$skorsatu = 1*$tampilsatu;
										$skordua = 2*$tampildua;
										$skortiga = 3*$tampiltiga;
										$skorempat = 4*$tampilempat;
										$skorlima = 5*$tampillima;*/
												//$skorenam = 5*$tampilenam;
										/*=============================================================*/
										$skorsatu = 1*$tampilsatu;
										$skordua = 2*$tampildua;
										$skortiga = 3*$tampiltiga;
										$skorempat = 4*$tampilempat;
										$skorlima = 5*$tampillima;
										$responden = ($tampil/54);
										$jumlah = ($skorsatu+$skordua+$skortiga+$skorempat+$skorlima);/*+$skorenam*/
										//test menggunakan tahapan yang ada diexcel
										//$rata = ($jumlah/$jumlahpertanyaan)
										// *coba tahap 1 karna gak tembus skala 1* $rata = (($jumlah/2)/($jumlahpertanyaan/2)/10);
										$jmlpertanyaan = ($jumlahpertanyaan/2);//ini yang 27 pertanyaan
										/*$total = $jumlah/2;
										$total = $total/$responden;
										$total = $total/$jmlpertanyaan;
										*/
										$rata = $jumlah/$jumlahpertanyaan;
										
										if ($rata !=0 && $rata <=1.99){
											$ratakes = "1";
										}else if ($rata <=2.99){
											$ratakes = "2";
										}else if($rata <=3.99){
											$ratakes = "3";
										}else if ($rata <=4.99){
											$ratakes = "4";
										}else if ($rata <=5.99){
											$ratakes = "5";
										}else if ($rata >5.99){
											$ratakes = "5";
										}
										
										if ($ratakes == 0){
											$ket= "Organisasi berada pada level <b>Non-Exist/AddHoc<b>";
										}else if($ratakes == 1){
											$ket= "Organisasi berada pada level <b>Initial<b>";
										}else if($ratakes == 2){
											$ket= "Organisasi berada pada level <b>Repeatable but intuitive<b>";
										}else if($ratakes == 3){
											$ket= "Organisasi berada pada level <b>Defined<b>";
										}else if($ratakes == 4){
											$ket= "Organisasi berada pada level <b>Managed<b>";
										}else if($ratakes == 5){
											$ket= "Organisasi berada pada level <b>Optimised<b>";
										}
										
									?>
										<!--<tr><th>Nilai Sebelum Dibulatkan</th><th><?php// echo $rata?></th></tr>-->
										<th>4</th>
										<th>Nilai Maturity :<?php echo $ratakes;?></th>
									</tr>
									<tr>
										<th>5</th>
										<th>Keterangan :<?php echo $ket;?></th>
									</tr>
								</thead>
							</table>
							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="container">
			<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12">
					<center><h4><b>Hasil Rekapitulasi Domain Service Strategy</b></h4></center><br><br>
						<div class="table-responsive">
							<form method="post"  action="<?php echo site_url('admin/c_rekapitulasi/aksi_tambahai'); ?>">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>1</th>
										<th>Nama Domain : SS</th>
									</tr>
									<tr>
										<th>2</th>
										<th>Jumlah Responden : <?php echo $ss_tampil/54;?></th>
									</tr>
									<tr>
										<th>3</th>
										<th>Jumlah Pertanyaan :<?php echo $ss_jumlahpertanyaan;?></th>
									</tr>
									<tr>
										<th>4</th>
										<th>Jumlah Jawaban
											<ol>
												<li>Jawaban 1 : <?php echo $ss_tampilsatu;?></li>
												<li>Jawaban 2 : <?php echo $ss_tampildua;?></li>
												<li>Jawaban 3 : <?php echo $ss_tampiltiga;?></li>
												<li>Jawaban 4 : <?php echo $ss_tampilempat;?></li>
												<li>Jawaban 5 : <?php echo $ss_tampillima;?></li>
											</ol>
										</th>
									</tr>
									<tr>
									<?php
										
										$ss_skorsatu = 1*$ss_tampilsatu;
										$ss_skordua = 2*$ss_tampildua;
										$ss_skortiga = 3*$ss_tampiltiga;
										$ss_skorempat = 4*$ss_tampilempat;
										$ss_skorlima = 5*$ss_tampillima;
										
										$ss_jumlah = ($ss_skorsatu+$ss_skordua+$ss_skortiga+$ss_skorempat+$ss_skorlima);
										$ss_rata = ($ss_jumlah/$ss_jumlahpertanyaan);
										
										if ($ss_rata !=0 && $rata <=1.99){
											$ratass = "1";
										}else if ($rata <=2.99){
											$ratass = "2";
										}else if($rata <=3.99){
											$ratass = "3";
										}else if ($rata <=4.99){
											$ratass = "4";
										}else if ($rata <=5.99){
											$ratass = "5";
										}else if ($rata >5.99){
											$ratass = "5";
										}
										
										if ($ratass == 0){
											$ketss="Organisasi berada pada level Non-Exist/AddHoc";
										}else if($ratass == 1){
											$ketss="Organisasi berada pada level Initial";
										}else if($ratass == 2){
											$ketss="Organisasi berada pada level Repeatable but intuitive";
										}else if($ratass == 3){
											$ketss="Organisasi berada pada level Defined";
										}else if($ratass == 4){
											$ketss="Organisasi berada pada level Managed";
										}else if($ratass == 5){
											$ketss="Organisasi berada pada level Optimised";
										}
									?>
										
										<th>5</th>
										<th>Nilai Maturity :<?php  echo $ratass;?></th>
									</tr>
									<tr>
										<th>6</th>
										<th>Keterangan :<?php  echo $ketss;?></th>
									</tr>
								</thead>
							</table>
							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12">
					<center><h4><b>Hasil Rekapitulasi Domain Service Design</b></h4></center><br><br>
						<div class="table-responsive">
							<form method="post"  action="<?php echo site_url('admin/c_rekapitulasi/aksi_tambahai'); ?>">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>1</th>
										<th>Nama Domain : SD</th>
									</tr>
									<tr>
										<th>2</th>
										<th>Jumlah Responden : <?php echo $sd_tampil/54;?></th>
									</tr>
									<tr>
										<th>3</th>
										<th>Jumlah Pertanyaan :<?php echo $sd_jumlahpertanyaan;?></th>
									</tr>
									<tr>
										<th>4</th>
										<th>Jumlah Jawaban
											<ol>
												<li>Jawaban 1 : <?php echo $sd_tampilsatu;?></li>
												<li>Jawaban 2 : <?php echo $sd_tampildua;?></li>
												<li>Jawaban 3 : <?php echo $sd_tampiltiga;?></li>
												<li>Jawaban 4 : <?php echo $sd_tampilempat;?></li>
												<li>Jawaban 5 : <?php echo $sd_tampillima;?></li>
												<!--<li>Jawaban 6 : <?php// echo $ss_tampilenam;?></li>-->
											</ol>
										</th>
									</tr>
									<tr>
									<?php
										
										$sd_skorsatu = 1*$sd_tampilsatu;
										$sd_skordua = 2*$sd_tampildua;
										$sd_skortiga = 3*$sd_tampiltiga;
										$sd_skorempat = 4*$sd_tampilempat;
										$sd_skorlima = 5*$sd_tampillima;
										
										$sd_jumlah = ($sd_skorsatu+$sd_skordua+$sd_skortiga+$sd_skorempat+$sd_skorlima);
										$sd_rata = ($sd_jumlah/$sd_jumlahpertanyaan);
										
										if ($sd_rata !=0 && $sd_rata <=1.99){
											$ratasd = "1";
										}else if ($sd_rata <=2.99){
											$ratasd = "2";
										}else if($sd_rata <=3.99){
											$ratasd = "3";
										}else if ($sd_rata <=4.99){
											$ratasd = "4";
										}else if ($sd_rata <=5.99){
											$ratasd = "5";
										}else if ($sd_rata >5.99){
											$ratasd = "5";
										}
									
										if ($ratasd == 0){
											$ketsd="Organisasi berada pada level Non-Exist/AddHoc";
										}else if($ratasd == 1){
											$ketsd="Organisasi berada pada level Initial";
										}else if($ratasd == 2){
											$ketsd="Organisasi berada pada level Repeatable but intuitive";
										}else if($ratasd == 3){
											$ketsd="Organisasi berada pada level Defined";
										}else if($ratasd == 4){
											$ketsd="Organisasi berada pada level Managed";
										}else if($ratasd == 5){
											$ketsd="Organisasi berada pada level Optimised";
										}
									?>
										
										<th>5</th>
										<th>Nilai Maturity :<?php  echo $ratasd;?></th>
									</tr>
									<tr>
										<th>6</th>
										<th>Keterangan :<?php  echo $ketsd;?></th>
									</tr>
								</thead>
							</table>
							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="container">
			<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12">
					<center><h4><b>Hasil Rekapitulasi Domain Service Transition</b></h4></center><br><br>
						<div class="table-responsive">
							<form method="post"  action="<?php echo site_url('admin/c_rekapitulasi/aksi_tambahai'); ?>">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>1</th>
										<th>Nama Domain : ST</th>
									</tr>
									<tr>
										<th>2</th>
										<th>Jumlah Responden : <?php echo $st_tampil/54;?></th>
									</tr>
									<tr>
										<th>3</th>
										<th>Jumlah Pertanyaan :<?php echo $st_jumlahpertanyaan;?></th>
									</tr>
									<tr>
										<th>4</th>
										<th>Jumlah Jawaban
											<ol>
												<li>Jawaban 1 : <?php echo $st_tampilsatu;?></li>
												<li>Jawaban 2 : <?php echo $st_tampildua;?></li>
												<li>Jawaban 3 : <?php echo $st_tampiltiga;?></li>
												<li>Jawaban 4 : <?php echo $st_tampilempat;?></li>
												<li>Jawaban 5 : <?php echo $st_tampillima;?></li>
												<!--<li>Jawaban 6 : <?php// echo $ss_tampilenam;?></li>-->
											</ol>
										</th>
									</tr>
									<tr>
									<?php
										
										$st_skorsatu = 1*$st_tampilsatu;
										$st_skordua = 2*$st_tampildua;
										$st_skortiga = 3*$st_tampiltiga;
										$st_skorempat = 4*$st_tampilempat;
										$st_skorlima = 5*$st_tampillima;
										
										$st_jumlah = ($st_skorsatu+$st_skordua+$st_skortiga+$st_skorempat+$st_skorlima);
										$st_rata = ($st_jumlah/$st_jumlahpertanyaan);
										
										if ($st_rata !=0 && $st_rata <=1.99){
											$ratast = "1";
										}else if ($st_rata <=2.99){
											$ratast = "2";
										}else if($st_rata <=3.99){
											$ratast = "3";
										}else if ($st_rata <=4.99){
											$ratast = "4";
										}else if ($st_rata <=5.99){
											$ratast = "5";
										}else if ($st_rata >5.99){
											$ratast = "5";
										}
										/*
										if ($ai_rata <=0.99){
											$rataai = "0";
										}else if ($ai_rata <=1.99){
											$rataai = "1";
										}else if($ai_rata <=2.99){
											$rataai = "2";
										}else if ($ai_rata <=3.99){
											$rataai = "3";
										}else if ($ai_rata <=4.99){
											$rataai = "4";
										}else if ($ai_rata <=5.99){
											$rataai = "5";
										}*/
										
										if ($ratast == 0){
											$ketst="Organisasi berada pada level Non-Exist/AddHoc";
										}else if($ratast == 1){
											$ketst="Organisasi berada pada level Initial";
										}else if($ratast == 2){
											$ketst="Organisasi berada pada level Repeatable but intuitive";
										}else if($ratast == 3){
											$ketst="Organisasi berada pada level Defined";
										}else if($ratast == 4){
											$ketst="Organisasi berada pada level Managed";
										}else if($ratast == 5){
											$ketst="Organisasi berada pada level Optimised";
										}
									?>
										
										<th>5</th>
										<th>Nilai Maturity :<?php  echo $ratast;?></th>
									</tr>
									<tr>
										<th>6</th>
										<th>Keterangan :<?php  echo $ketst;?></th>
									</tr>
								</thead>
							</table>
							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12">
					<center><h4><b>Hasil Rekapitulasi Domain Service Operation</b></h4></center><br><br>
						<div class="table-responsive">
							<form method="post"  action="<?php echo site_url('admin/c_rekapitulasi/aksi_tambahai'); ?>">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>1</th>
										<th>Nama Domain : SO</th>
									</tr>
									<tr>
										<th>2</th>
										<th>Jumlah Responden : <?php echo $so_tampil/54;?></th>
									</tr>
									<tr>
										<th>3</th>
										<th>Jumlah Pertanyaan :<?php echo $so_jumlahpertanyaan;?></th>
									</tr>
									<tr>
										<th>4</th>
										<th>Jumlah Jawaban
											<ol>
												<li>Jawaban 1 : <?php echo $so_tampilsatu;?></li>
												<li>Jawaban 2 : <?php echo $so_tampildua;?></li>
												<li>Jawaban 3 : <?php echo $so_tampiltiga;?></li>
												<li>Jawaban 4 : <?php echo $so_tampilempat;?></li>
												<li>Jawaban 5 : <?php echo $so_tampillima;?></li>
												<!--<li>Jawaban 6 : <?php// echo $ss_tampilenam;?></li>-->
											</ol>
										</th>
									</tr>
									<tr>
									<?php
										
										$so_skorsatu = 1*$so_tampilsatu;
										$so_skordua = 2*$so_tampildua;
										$so_skortiga = 3*$so_tampiltiga;
										$so_skorempat = 4*$so_tampilempat;
										$so_skorlima = 5*$so_tampillima;
										
										$so_jumlah = ($so_skorsatu+$so_skordua+$so_skortiga+$so_skorempat+$so_skorlima);
										$so_rata = ($so_jumlah/$so_jumlahpertanyaan);
										
										if ($so_rata !=0 && $so_rata <=1.99){
											$rataso = "1";
										}else if ($so_rata <=2.99){
											$rataso = "2";
										}else if($so_rata <=3.99){
											$rataso = "3";
										}else if ($so_rata <=4.99){
											$rataso = "4";
										}else if ($so_rata <=5.99){
											$rataso = "5";
										}else if ($so_rata >5.99){
											$rataso = "5";
										}
										/*
										if ($ai_rata <=0.99){
											$rataai = "0";
										}else if ($ai_rata <=1.99){
											$rataai = "1";
										}else if($ai_rata <=2.99){
											$rataai = "2";
										}else if ($ai_rata <=3.99){
											$rataai = "3";
										}else if ($ai_rata <=4.99){
											$rataai = "4";
										}else if ($ai_rata <=5.99){
											$rataai = "5";
										}*/
										
										if ($rataso == 0){
											$ketso="Organisasi berada pada level Non-Exist/AddHoc";
										}else if($rataso == 1){
											$ketso="Organisasi berada pada level Initial";
										}else if($rataso == 2){
											$ketso="Organisasi berada pada level Repeatable but intuitive";
										}else if($rataso == 3){
											$ketso="Organisasi berada pada level Defined";
										}else if($rataso == 4){
											$ketso="Organisasi berada pada level Managed";
										}else if($rataso == 5){
											$ketso="Organisasi berada pada level Optimised";
										}
									?>
										
										<th>5</th>
										<th>Nilai Maturity :<?php  echo $rataso;?></th>
									</tr>
									<tr>
										<th>6</th>
										<th>Keterangan :<?php  echo $ketso;?></th>
									</tr>
								</thead>
							</table>
							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="container">
			<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12">
					<center><h4><b>Hasil Rekapitulasi Domain Continual Service Improvement</b></h4></center><br><br>
						<div class="table-responsive">
							<form method="post"  action="<?php echo site_url('admin/c_rekapitulasi/aksi_tambahai'); ?>">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>1</th>
										<th>Nama Domain : CSI</th>
									</tr>
									<tr>
										<th>2</th>
										<th>Jumlah Responden : <?php echo $csi_tampil/54;?></th>
									</tr>
									<tr>
										<th>3</th>
										<th>Jumlah Pertanyaan :<?php echo $csi_jumlahpertanyaan;?></th>
									</tr>
									<tr>
										<th>4</th>
										<th>Jumlah Jawaban
											<ol>
												<li>Jawaban 1 : <?php echo $csi_tampilsatu;?></li>
												<li>Jawaban 2 : <?php echo $csi_tampildua;?></li>
												<li>Jawaban 3 : <?php echo $csi_tampiltiga;?></li>
												<li>Jawaban 4 : <?php echo $csi_tampilempat;?></li>
												<li>Jawaban 5 : <?php echo $csi_tampillima;?></li>
												<!--<li>Jawaban 6 : <?php// echo $ss_tampilenam;?></li>-->
											</ol>
										</th>
									</tr>
									<tr>
									<?php
										
										$csi_skorsatu = 1*$csi_tampilsatu;
										$csi_skordua = 2*$csi_tampildua;
										$csi_skortiga = 3*$csi_tampiltiga;
										$csi_skorempat = 4*$csi_tampilempat;
										$csi_skorlima = 5*$csi_tampillima;
										
										$csi_jumlah = ($csi_skorsatu+$csi_skordua+$csi_skortiga+$csi_skorempat+$csi_skorlima);
										$csi_rata = ($csi_jumlah/$csi_jumlahpertanyaan);
										
										if ($csi_rata !=0 && $csi_rata <=1.99){
											$ratacsi = "1";
										}else if ($csi_rata <=2.99){
											$ratacsi = "2";
										}else if($csi_rata <=3.99){
											$ratacsi = "3";
										}else if ($csi_rata <=4.99){
											$ratacsi = "4";
										}else if ($csi_rata <=5.99){
											$ratacsi = "5";
										}else if ($csi_rata >5.99){
											$ratacsi = "5";
										}
										/*
										if ($ai_rata <=0.99){
											$rataai = "0";
										}else if ($ai_rata <=1.99){
											$rataai = "1";
										}else if($ai_rata <=2.99){
											$rataai = "2";
										}else if ($ai_rata <=3.99){
											$rataai = "3";
										}else if ($ai_rata <=4.99){
											$rataai = "4";
										}else if ($ai_rata <=5.99){
											$rataai = "5";
										}*/
										
										if ($ratacsi == 0){
											$ketcsi="Organisasi berada pada level Non-Exist/AddHoc";
										}else if($ratacsi == 1){
											$ketcsi="Organisasi berada pada level Initial";
										}else if($ratacsi == 2){
											$ketcsi="Organisasi berada pada level Repeatable but intuitive";
										}else if($ratacsi == 3){
											$ketcsi="Organisasi berada pada level Defined";
										}else if($ratacsi == 4){
											$ketcsi="Organisasi berada pada level Managed";
										}else if($ratacsi == 5){
											$ketcsi="Organisasi berada pada level Optimised";
										}
									?>
										
										<th>5</th>
										<th>Nilai Maturity :<?php  echo $ratacsi;?></th>
									</tr>
									<tr>
										<th>6</th>
										<th>Keterangan :<?php  echo $ketcsi;?></th>
									</tr>
								</thead>
							</table>
							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- js placed at the end of the document so the pages load faster -->
		<script src="<?php echo base_url()?>asset/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url()?>asset/js/jquery.js"></script>
	</body>
</html>