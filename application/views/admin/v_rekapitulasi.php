<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Auditor-Rekapitulasi</title>
	
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
	
		<link href="<?php echo base_url('asset/css/bootstrap.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('asset/css/bootstrap.min.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('asset/font-awesome/css/font-awesome.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('asset/csss/plugins/morris.css');?>" rel="stylesheet">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/login.css" rel="stylesheet">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/freelancer.css" rel="stylesheet">
            
		<script src="<?php echo base_url('asset/js/jquery.js');?>"></script>
		<script src="<?php echo base_url('asset/js/bootstrap.js');?>"></script>
		<script src="<?php echo base_url('asset/js/tinymce/tinymce.min.js');?>"></script>
		<script>
			tinymce.init({selector:'textarea'});
		</script>
	</head>
	
	<body>
		<center><img src="<?php echo base_url('asset/img/fti.png');?>" height="100%" width="100%"></center>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-collapse collapse">
					<ul style="font-color:black;" class="nav navbar-nav navbar-left">
						<li class="btn"><a href="<?php echo base_url('admin/c_auditor');?>"><span class="text"><i class="fa fa-home"></i>&nbsp;Home</span></a></li>
						<li class="btn"><a href="<?php echo base_url('admin/c_kelolapengguna');?>"><span class="text"><i class="fa fa-user"></i>&nbsp;Kelola Pengguna</span></a></li>
						<li class="btn"><a href="<?php echo base_url('admin/c_kelolakuesioner');?>"><span class="text"><i class="fa fa-fw fa-edit"></i>&nbsp;Kelola Kuesioner</span></a></li>
						<li class="btn"><a href="<?php echo base_url('admin/c_rekapitulasi');?>"><span class="text"><i class="fa fa-fw fa-bar-chart-o"></i>&nbsp;Rekapitulasi</span></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="btn"><a href="<?php echo base_url('c_login/logout');?>"><span class="text"><i class="fa fa-fw fa-power-off"></i>&nbsp;Logout</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12">
					<center><h4><b>Hasil Rekapitulasi Keseluruhan</b></h4></center><br><br>
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>1</th>
										<th>Jumlah Responden : <?php echo ($tampil/54);?></th>
									</tr>
									<tr>
										<th>2</th>
										<th>Jumlah Pertanyaan :<?php echo $jumlahpertanyaan;?></th>
									</tr>
										<tr>
										<th>3</th>
										<th>Jumlah Jawaban
											<ol>
												<li>Jawaban 1 : <?php echo $tampilsatu;?></li>
												<li>Jawaban 2 : <?php echo $tampildua;?></li>
												<li>Jawaban 3 : <?php echo $tampiltiga;?></li>
												<li>Jawaban 4 : <?php echo $tampilempat;?></li>
												<li>Jawaban 5 : <?php echo $tampillima;?></li>
												<!--<li>Jawaban 6 : <?php// echo $tampilenam;?></li>-->
											</ol>
										</th>
									</tr>
									<tr>
									<?php
										$skorsatu = 1*$tampilsatu;
										$skordua = 2*$tampildua;
										$skortiga = 3*$tampiltiga;
										$skorempat = 4*$tampilempat;
										$skorlima = 5*$tampillima;
										//$skorenam = 5*$tampilenam;
										$jumlah = ($skorsatu+$skordua+$skortiga+$skorempat+$skorlima);
										$rata = ($jumlah/$jumlahpertanyaan);
										if ($rata <=0.99){
											$ratakes = "0";
										}else if ($rata <=1.99){
											$ratakes = "1";
										}else if($rata <=2.99){
											$ratakes = "2";
										}else if ($rata <=3.99){
											$ratakes = "3";
										}else if ($rata <=4.99){
											$ratakes = "4";
										}else if ($rata <=5.99){
											$ratakes = "5";
										}
										
										if ($ratakes == 0){
											$ket="Organisasi berada pada level <b>Non-Exist/AddHoc<b>";
										}else if($ratakes == 1){
											$ket="Organisasi berada pada level <b>Initial<b>";
										}else if($ratakes == 2){
											$ket="Organisasi berada pada level <b>Repeatable but intuitive<b>";
										}else if($ratakes == 3){
											$ket="Organisasi berada pada level <b>Defined<b>";
										}else if($ratakes == 4){
											$ket="Organisasi berada pada level <b>Managed<b>";
										}else if($ratakes == 5){
											$ket="Organisasi berada pada level <b>Optimised<b>";
										}
										
									?>
										<th>4</th>
										<th>Nilai Maturity :<?php echo $ratakes;?></th>
									</tr>
									<tr>
										<th>5</th>
										<th>Keterangan :<?php echo $ket;?></th>
									</tr>
								</thead>
							</table>
							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="container">
			<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12">
					<center><h4><b>Hasil Rekapitulasi Domain AI</b></h4></center><br><br>
						<div class="table-responsive">
							<form method="post"  action="<?php echo site_url('admin/c_rekapitulasi/aksi_tambahai'); ?>">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>1</th>
										<th>Nama Domain : AI</th>
									</tr>
									<tr>
										<th>2</th>
										<th>Jumlah Responden : <?php echo $ai_tampil;?></th>
									</tr>
									<tr>
										<th>3</th>
										<th>Jumlah Pertanyaan :<?php echo $ai_jumlahpertanyaan;?></th>
									</tr>
									<tr>
										<th>4</th>
										<th>Jumlah Jawaban
											<ol>
												<li>Jawaban 1 : <?php echo $ai_tampilsatu;?></li>
												<li>Jawaban 2 : <?php echo $ai_tampildua;?></li>
												<li>Jawaban 3 : <?php echo $ai_tampiltiga;?></li>
												<li>Jawaban 4 : <?php echo $ai_tampilempat;?></li>
												<li>Jawaban 5 : <?php echo $ai_tampillima;?></li>
												<li>Jawaban 6 : <?php echo $ai_tampilenam;?></li>
											</ol>
										</th>
									</tr>
									<tr>
									<?php
										$ai_skorsatu = 0*$ai_tampilsatu;
										$ai_skordua = 1*$ai_tampildua;
										$ai_skortiga = 2*$ai_tampiltiga;
										$ai_skorempat = 3*$ai_tampilempat;
										$ai_skorlima = 4*$ai_tampillima;
										$ai_skorenam = 5*$ai_tampilenam;
										
										$ai_jumlah = ($ai_skorsatu+$ai_skordua+$ai_skortiga+$ai_skorempat+$ai_skorlima+$ai_skorenam);
										$ai_rata = ($ai_jumlah/$ai_jumlahpertanyaan);
									
										if ($ai_rata <=0.99){
											$rataai = "0";
										}else if ($ai_rata <=1.99){
											$rataai = "1";
										}else if($ai_rata <=2.99){
											$rataai = "2";
										}else if ($ai_rata <=3.99){
											$rataai = "3";
										}else if ($ai_rata <=4.99){
											$rataai = "4";
										}else if ($ai_rata <=5.99){
											$rataai = "5";
										}
										
										if ($rataai == 0){
											$ketai="Organisasi berada pada level Non-Exist/AddHoc";
										}else if($rataai == 1){
											$ketai="Organisasi berada pada level Initial";
										}else if($rataai == 2){
											$ketai="Organisasi berada pada level Repeatable but intuitive";
										}else if($rataai == 3){
											$ketai="Organisasi berada pada level Defined";
										}else if($rataai == 4){
											$ketai="Organisasi berada pada level Managed";
										}else if($rataai == 5){
											$ketai="Organisasi berada pada level Optimised";
										}
									?>
									 
										<th>5</th>
										<th>Nilai Maturity :<?php  echo $rataai;?></th>
									</tr>
									<tr>
										<th>6</th>
										<th>Keterangan :<?php  echo $ketai;?></th>
									</tr>
								</thead>
							</table>
							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12">
					<center><h4><b>Hasil Rekapitulasi Domain DS</b></h4></center><br><br>
						<div class="table-responsive">
							<form method="post"  action="<?php echo site_url('admin/c_rekapitulasi/aksi_tambahds'); ?>">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>1</th>
										<th>Nama Domain :DS</th>
									</tr>
									<tr>
										<th>2</th>
										<th>Jumlah Responden : <?php echo $ds_tampil;?></th>
									</tr>
									<tr>
										<th>3</th>
										<th>Jumlah Pertanyaan :<?php echo $ds_jumlahpertanyaan;?></th>
									</tr>
									<tr>
										<th>4</th>
										<th>Jumlah Jawaban
											<ol>
												<li>Jawaban 1 : <?php echo $ds_tampilsatu;?></li>
												<li>Jawaban 2 : <?php echo $ds_tampildua;?></li>
												<li>Jawaban 3 : <?php echo $ds_tampiltiga;?></li>
												<li>Jawaban 4 : <?php echo $ds_tampilempat;?></li>
												<li>Jawaban 5 : <?php echo $ds_tampillima;?></li>
												<li>Jawaban 6 : <?php echo $ds_tampilenam;?></li>
											</ol>
										</th>
									</tr>
									<tr>
									<?php
										$ds_skorsatu = 0*$ds_tampilsatu;
										$ds_skordua = 1*$ds_tampildua;
										$ds_skortiga = 2*$ds_tampiltiga;
										$ds_skorempat = 3*$ds_tampilempat;
										$ds_skorlima = 4*$ds_tampillima;
										$ds_skorenam = 5*$ds_tampilenam;
										
										$ds_jumlah = ($ds_skorsatu+$ds_skordua+$ds_skortiga+$ds_skorempat+$ds_skorlima+$ds_skorenam);
										$ds_rata = ($ds_jumlah/$ds_jumlahpertanyaan);
									
										if ($ds_rata <=0.99){
											$ratads = "0";
										}else if ($ds_rata <=1.99){
											$ratads = "1";
										}else if($ds_rata <=2.99){
											$ratads = "2";
										}else if ($ds_rata <=3.99){
											$ratads = "3";
										}else if ($ds_rata <=4.99){
											$ratads = "4";
										}else if ($ds_rata <=5.99){
											$ratads = "5";
										}
										
										if ($ratads == 0){
											$ketds="Organisasi berada pada level Non-Exist/AddHoc";
										}else if($ratads == 1){
											$ketds="Organisasi berada pada level Initial";
										}else if($ratads == 2){
											$ketds="Organisasi berada pada level Repeatable but intuitive";
										}else if($ratads == 3){
											$ketds="Organisasi berada pada level Defined";
										}else if($ratads == 4){
											$ketds="Organisasi berada pada level Managed";
										}else if($ratads == 5){
											$ketds="Organisasi berada pada level Optimised";
										}
									?>
									 
										<th>5</th>
										<th>Nilai Maturity :<?php  echo $ratads;?></th>
									</tr>
									<tr>
										<th>6</th>
										<th>Keterangan :<?php  echo $ketds;?></th>
									</tr>
								</thead>
							</table>
							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12">
					<center><h4><b>Hasil Rekapitulasi Domain ME</b></h4></center><br><br>
						<div class="table-responsive">
							<form method="post"  action="<?php echo site_url('admin/c_rekapitulasi/aksi_tambahme'); ?>">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>1</th>
										<th>Nama Domain :ME</th>
									</tr>
									<tr>
										<th>2</th>
										<th>Jumlah Responden : <?php echo $me_tampil;?></th>
									</tr>
									<tr>
										<th>3</th>
										<th>Jumlah Pertanyaan :<?php echo $me_jumlahpertanyaan;?></th>
									</tr>
									<tr>
										<th>4</th>
										<th>Jumlah Jawaban
											<ol>
												<li>Jawaban 1 : <?php echo $me_tampilsatu;?></li>
												<li>Jawaban 2 : <?php echo $me_tampildua;?></li>
												<li>Jawaban 3 : <?php echo $me_tampiltiga;?></li>
												<li>Jawaban 4 : <?php echo $me_tampilempat;?></li>
												<li>Jawaban 5 : <?php echo $me_tampillima;?></li>
												<li>Jawaban 6 : <?php echo $me_tampilenam;?></li>
											</ol>
										</th>
									</tr>
									<tr>
									<?php
										$me_skorsatu = 0*$me_tampilsatu;
										$me_skordua = 1*$me_tampildua;
										$me_skortiga = 2*$me_tampiltiga;
										$me_skorempat = 3*$me_tampilempat;
										$me_skorlima = 4*$me_tampillima;
										$me_skorenam = 5*$me_tampilenam;
										
										$me_jumlah = ($me_skorsatu+$me_skordua+$me_skortiga+$me_skorempat+$me_skorlima+$me_skorenam);
										$me_rata = ($me_jumlah/$me_jumlahpertanyaan);
									
										if ($me_rata <=0.99){
											$ratame = "0";
										}else if ($me_rata <=1.99){
											$ratame = "1";
										}else if($me_rata <=2.99){
											$ratame = "2";
										}else if ($me_rata <=3.99){
											$ratame = "3";
										}else if ($me_rata <=4.99){
											$ratame = "4";
										}else if ($me_rata <=5.99){
											$ratame = "5";
										}
										
										if ($ratame == 0){
											$ketme="Organisasi berada pada level Non-Exist/AddHoc";
										}else if($ratame == 1){
											$ketme="Organisasi berada pada level Initial";
										}else if($ratame == 2){
											$ketme="Organisasi berada pada level Repeatable but intuitive";
										}else if($ratame == 3){
											$ketme="Organisasi berada pada level Defined";
										}else if($ratame == 4){
											$ketme="Organisasi berada pada level Managed";
										}else if($ratame == 5){
											$ketme="Organisasi berada pada level Optimised";
										}
									?>
									 
										<th>5</th>
										<th>Nilai Maturity :<?php  echo $ratame;?></th>
									</tr>
									<tr>
										<th>6</th>
										<th>Keterangan :<?php  echo $ketme;?></th>
									</tr>
								</thead>
							</table>
							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12">
					<center><h4><b>Hasil Rekapitulasi Domain AI</b></h4></center><br><br>
						<div class="table-responsive">
							<form method="post"  action="<?php echo site_url('admin/c_rekapitulasi/aksi_tambahai'); ?>">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>1</th>
										<th>Nama Domain : AI</th>
									</tr>
									<tr>
										<th>2</th>
										<th>Jumlah Responden : <?php echo $ai_tampil;?></th>
									</tr>
									<tr>
										<th>3</th>
										<th>Jumlah Pertanyaan :<?php echo $ai_jumlahpertanyaan;?></th>
									</tr>
									<tr>
										<th>4</th>
										<th>Jumlah Jawaban
											<ol>
												<li>Jawaban 1 : <?php echo $ai_tampilsatu;?></li>
												<li>Jawaban 2 : <?php echo $ai_tampildua;?></li>
												<li>Jawaban 3 : <?php echo $ai_tampiltiga;?></li>
												<li>Jawaban 4 : <?php echo $ai_tampilempat;?></li>
												<li>Jawaban 5 : <?php echo $ai_tampillima;?></li>
												<li>Jawaban 6 : <?php echo $ai_tampilenam;?></li>
											</ol>
										</th>
									</tr>
									<tr>
									<?php
										$ai_skorsatu = 0*$ai_tampilsatu;
										$ai_skordua = 1*$ai_tampildua;
										$ai_skortiga = 2*$ai_tampiltiga;
										$ai_skorempat = 3*$ai_tampilempat;
										$ai_skorlima = 4*$ai_tampillima;
										$ai_skorenam = 5*$ai_tampilenam;
										
										$ai_jumlah = ($ai_skorsatu+$ai_skordua+$ai_skortiga+$ai_skorempat+$ai_skorlima+$ai_skorenam);
										$ai_rata = ($ai_jumlah/$ai_jumlahpertanyaan);
									
										if ($ai_rata <=0.99){
											$rataai = "0";
										}else if ($ai_rata <=1.99){
											$rataai = "1";
										}else if($ai_rata <=2.99){
											$rataai = "2";
										}else if ($ai_rata <=3.99){
											$rataai = "3";
										}else if ($ai_rata <=4.99){
											$rataai = "4";
										}else if ($ai_rata <=5.99){
											$rataai = "5";
										}
										
										if ($rataai == 0){
											$ketai="Organisasi berada pada level Non-Exist/AddHoc";
										}else if($rataai == 1){
											$ketai="Organisasi berada pada level Initial";
										}else if($rataai == 2){
											$ketai="Organisasi berada pada level Repeatable but intuitive";
										}else if($rataai == 3){
											$ketai="Organisasi berada pada level Defined";
										}else if($rataai == 4){
											$ketai="Organisasi berada pada level Managed";
										}else if($rataai == 5){
											$ketai="Organisasi berada pada level Optimised";
										}
									?>
									 
										<th>5</th>
										<th>Nilai Maturity :<?php  echo $rataai;?></th>
									</tr>
									<tr>
										<th>6</th>
										<th>Keterangan :<?php  echo $ketai;?></th>
									</tr>
								</thead>
							</table>
							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="jumbotron">
				<div class="row">
					<div class="col-lg-12">
					<center><h4><b>Hasil Rekapitulasi Domain AI</b></h4></center><br><br>
						<div class="table-responsive">
							<form method="post"  action="<?php echo site_url('admin/c_rekapitulasi/aksi_tambahai'); ?>">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>1</th>
										<th>Nama Domain : AI</th>
									</tr>
									<tr>
										<th>2</th>
										<th>Jumlah Responden : <?php echo $ai_tampil;?></th>
									</tr>
									<tr>
										<th>3</th>
										<th>Jumlah Pertanyaan :<?php echo $ai_jumlahpertanyaan;?></th>
									</tr>
									<tr>
										<th>4</th>
										<th>Jumlah Jawaban
											<ol>
												<li>Jawaban 1 : <?php echo $ai_tampilsatu;?></li>
												<li>Jawaban 2 : <?php echo $ai_tampildua;?></li>
												<li>Jawaban 3 : <?php echo $ai_tampiltiga;?></li>
												<li>Jawaban 4 : <?php echo $ai_tampilempat;?></li>
												<li>Jawaban 5 : <?php echo $ai_tampillima;?></li>
												<li>Jawaban 6 : <?php echo $ai_tampilenam;?></li>
											</ol>
										</th>
									</tr>
									<tr>
									<?php
										$ai_skorsatu = 0*$ai_tampilsatu;
										$ai_skordua = 1*$ai_tampildua;
										$ai_skortiga = 2*$ai_tampiltiga;
										$ai_skorempat = 3*$ai_tampilempat;
										$ai_skorlima = 4*$ai_tampillima;
										$ai_skorenam = 5*$ai_tampilenam;
										
										$ai_jumlah = ($ai_skorsatu+$ai_skordua+$ai_skortiga+$ai_skorempat+$ai_skorlima+$ai_skorenam);
										$ai_rata = ($ai_jumlah/$ai_jumlahpertanyaan);
									
										if ($ai_rata <=0.99){
											$rataai = "0";
										}else if ($ai_rata <=1.99){
											$rataai = "1";
										}else if($ai_rata <=2.99){
											$rataai = "2";
										}else if ($ai_rata <=3.99){
											$rataai = "3";
										}else if ($ai_rata <=4.99){
											$rataai = "4";
										}else if ($ai_rata <=5.99){
											$rataai = "5";
										}
										
										if ($rataai == 0){
											$ketai="Organisasi berada pada level Non-Exist/AddHoc";
										}else if($rataai == 1){
											$ketai="Organisasi berada pada level Initial";
										}else if($rataai == 2){
											$ketai="Organisasi berada pada level Repeatable but intuitive";
										}else if($rataai == 3){
											$ketai="Organisasi berada pada level Defined";
										}else if($rataai == 4){
											$ketai="Organisasi berada pada level Managed";
										}else if($rataai == 5){
											$ketai="Organisasi berada pada level Optimised";
										}
									?>
									 
										<th>5</th>
										<th>Nilai Maturity :<?php  echo $rataai;?></th>
									</tr>
									<tr>
										<th>6</th>
										<th>Keterangan :<?php  echo $ketai;?></th>
									</tr>
								</thead>
							</table>
							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- js placed at the end of the document so the pages load faster -->
		<script src="<?php echo base_url()?>asset/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url()?>asset/js/jquery.js"></script>
	</body>
</html>