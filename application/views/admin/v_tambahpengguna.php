<!DOCTYPE html>
<html lang="en">
  <head>
	<title>Auditor-TambahUser</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	
	
	<link href="<?php echo base_url('asset/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/font-awesome/css/font-awesome.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/plugins/morris/morris-0.4.3.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/plugins/timeline/timeline.css');?>" rel="stylesheet">
        
            
    <script src="<?php echo base_url('asset/js/jquery.js');?>"></script>
    <script src="<?php echo base_url('asset/js/bootstrap.js');?>"></script>
    <script src="<?php echo base_url('asset/js/tinymce/tinymce.min.js');?>"></script>
    <script>
        tinymce.init({selector:'textarea'});
		
		/*function validasi(){
			var valnama = /^[a-zA-Z]+(([\'\,\.\-][a-zA-Z])?[a-zA-Z]*)*$/;
			var valangka = /^[0-9]*$/;
			var nama = formulir.username.value;
			
			if (nama != "" && !nama.match(valnama) && !nama.match(valangka)){
				alert('Username Tidak boleh diisi karakter!');
				return false;
			}
		}
    </script>
  </head>

  <body>
	<center><img src="<?php echo base_url('asset/img/fti.png');?>" height="100%" width="100%"></center>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-collapse collapse">
	            <ul style="font-color:black;" class="nav navbar-nav navbar-left">
					<li class="btn"><a href="<?php echo base_url('admin/c_auditor');?>"><span class="text"><i class="fa fa-home"></i>&nbsp;Home</span></a></li>
	                <li class="btn"><a href="<?php echo base_url('admin/c_kelolapengguna');?>"><span class="text"><i class="fa fa-user"></i>&nbsp;Kelola Pengguna</span></a></li>
	                <li class="btn"><a href="<?php echo base_url('admin/c_kelolakuesioner');?>"><span class="text"><i class="fa fa-fw fa-edit"></i>&nbsp;Kelola Kuesioner</span></a></li>
	                <li class="btn"><a href="<?php echo base_url('admin/c_rekapitulasi');?>"><span class="text"><i class="fa fa-fw fa-bar-chart-o"></i>&nbsp;Rekapitulasi</span></a></li>
	            </ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="btn"><a href="<?php echo base_url('c_login/logout');?>"><span class="text"><i class="fa fa-fw fa-power-off"></i>&nbsp;Logout</span></a></li>
	            </ul>
	        </div>
		</div>
	</nav>
	
	<div class="container">
		<div class="jumbotron col-sm-12">
			<div class="widget-title" align="center">
				<h4><b>Tambah Pengguna</b></h4>
			</div>
			<?/*onsubmit = "return validasi()"*/?>
			<form class="form-horizontal" name="formulir" method="post" action="<?php echo site_url('admin/c_kelolapengguna/aksi_tambahpengguna'); ?>">
				<div class="form-group">
					<label for="kodepengguna" style="color:black;float:left">Kode Pengguna</label>
					<input type="text" class="form-control"  placeholder="Kode Pengguna" name = "kodepengguna" required autofocus>
				</div>
				<div class="form-group">
					<label for="nim/kode_dosen" style="color:black;float:left">Nim/Kode Dosen</label>
					<input type="text" class="form-control" placeholder=" Nim/Kode Dosen" name = "username" required>
				</div>
				<div class="form-group">
					<label for="alamat" style="color:black;float:left">Password</label>
					<input type="password" class="form-control" placeholder="Password" name = "password" required >
				</div>
				<div class="form-group">
					<label for="nama" style="color:black;float:left">Nama</label>
					<input type="text" class="form-control" placeholder=" Nama" name = "nama" required>
				</div>
				<div class="form-group">
					<label for="level" style="color:black;float:left">Level</label>
					<select class = "form-control" name = "level" >
						<option value = "mahasiswa">Mahasiswa</option>
						<option value = "dosen">Dosen</option>
						
					</select>
				</div>
				<div class="modal-footer">
					<center>
						<input type="submit" class="btn btn-sm btn-primary" value="Simpan">
						<a href="<?php echo site_url('admin/c_kelolapengguna');?>" class="btn btn-sm btn-danger">Batal</a>
					</center>
				</div>
			</form>
		</div>
	</div>
<!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url()?>asset/js/bootstrap.js"></script>
    <script src="<?php echo base_url()?>asset/js/jquery.js"></script>
  </body>
</html>