<!DOCTYPE html>
<html lang="en">
  <head>
	<title>Login</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	
	<link href="<?php echo base_url('asset/css/bootstrap.css');?>" rel="stylesheet">
	<link href="<?php echo base_url('asset/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/font-awesome/css/font-awesome.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/plugins/morris.css');?>" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/login.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/freelancer.css" rel="stylesheet">
            
    <script src="<?php echo base_url('asset/js/jquery.js');?>"></script>
    <script src="<?php echo base_url('asset/js/bootstrap.js');?>"></script>
    <script src="<?php echo base_url('asset/js/tinymce/tinymce.min.js');?>"></script>
	<style type="text/css">
			body{
				background:url(asset/img/bg.jpg);
				background-size:cover;
				margin:0;
			}
			#container{
				position :relative;
				z-index:1;
				max-width:300px;
				margin:0 auto;
			}
			.login-page{
				width:360px;
				padding:5% 0 0;
				margin:auto; 
			}	
			.form{
				position:relative;
				z-index:1;
				background:#ffffff;
				max-width:360px;
				margin:0 auto 100px;
				padding:45px;
				text-align:center;
				box-shadow:0 0 20px 0 rgba(0,0,0,0.2),0 5px 5px 0 rgba(0,0,0,0.24);
			}
		</style>
  </head>

  <body>
	<center><img src="<?php echo base_url('asset/img/fti.png');?>" height="100%" width="100%"></center>
	<center><h2></h2></center>
    <div class="login-page" >
		<div id="container" >
			<div class="form" >
				<form action="<?php echo site_url('c_login/cek_login'); ?>" method="post"> 
					<div class="form-login-heading" align="center"><b>Sistem Evaluasi Bagian TA FTI UKSW</b></div><br><br>
					<div class="login-wrap">
						<input name="username" required="required" type="text" placeholder="username" class="form-control" autofocus><br>
						<input name="password" required="required" type="password" placeholder="password" class="form-control"><br>
						<input type="submit" name="login" value="Login" id="submit-login" class="btn btn-sm btn-primary" />      
					</div>
				</form>
			</div>
			<center><div id="footer"><p>© 2017</p><p>Fakultas Teknologi Informasi</p><p>Universitas Kristen Satya Wacana</p><p>Jl. Dr. O. Notohamidjojo, Blotongan, Salatiga</p></div></center>
		</div>
	</div>

<!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url()?>asset/js/bootstrap.js"></script>
    <script src="<?php echo base_url()?>asset/js/jquery.js"></script>
  </body>
</html>