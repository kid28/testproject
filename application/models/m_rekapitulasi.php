<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_rekapitulasi extends CI_Model {
		//baca responden keseluruhan
		public function baca_responden() {
			$a = $this->db->query('SELECT *FROM isikuesioner');
			$hitung = $a->num_rows();
			return $hitung;
		}
		//baca responden ai || ganti ke SS
		public function baca_respondenss(){
			$a = $this->db->query('SELECT *FROM isikuesioner');
			$hitung = $a->num_rows();
			return $hitung;
		}
		//baca responden me || ganti ke SD
		public function baca_respondensd(){
			$a = $this->db->query('SELECT *FROM isikuesioner');
			$hitung = $a->num_rows();
			return $hitung;
		}
		//baca responden ds || ganti ke ST
		public function baca_respondenst(){
			$a = $this->db->query('SELECT *FROM isikuesioner');
			$hitung = $a->num_rows();
			return $hitung;
		}
		//baca responden SO
		public function baca_respondenso(){
			$a = $this->db->query('SELECT *FROM isikuesioner');
			$hitung = $a->num_rows();
			return $hitung;
		}
		
		//baca responden CSI
		public function baca_respondencsi(){
			$a = $this->db->query('SELECT *FROM isikuesioner');
			$hitung = $a->num_rows();
			return $hitung;
		}
		
		//baca jawaban keseluruhan
		public function jumlahjawabansatu(){
			$a = $this->db->query('select *from isikuesioner where jawaban=1');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabandua(){
			$b = $this->db->query('select *from isikuesioner where jawaban=2');
			$hitung_b = $b->num_rows();
			return $hitung_b;
		}
		public function jumlahjawabantiga(){
			$c = $this->db->query('select *from isikuesioner where jawaban=3');
			$hitung_c = $c->num_rows();
			return $hitung_c;
		}
		public function jumlahjawabanempat(){
			$d = $this->db->query('select *from isikuesioner where jawaban=4');
			$hitung_d = $d->num_rows();
			return $hitung_d;
		}
		public function jumlahjawabanlima(){
			$e = $this->db->query('select *from isikuesioner where jawaban=5');
			$hitung_e = $e->num_rows();
			return $hitung_e;
		}
		public function jumlahjawabanenam(){
			$f = $this->db->query('select *from isikuesioner where jawaban=6');
			$hitung_f = $f->num_rows();
			return $hitung_f;
		}
		//baca jawaban SS
		public function jumlahjawabansssatu(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "s%" and jawaban=1');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabanssdua(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "s%" and jawaban=2');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansstiga(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "s%" and jawaban=3');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabanssempat(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "s%" and jawaban=4');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansslima(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "s%" and jawaban=5');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabanssenam(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "a%" and jawaban=6');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		
		//baca jawaban ds to sd
		public function jumlahjawabansdsatu(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "d%" and jawaban=1');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansddua(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "d%" and jawaban=2');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansdtiga(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "d%" and jawaban=3');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansdempat(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "d%" and jawaban=4');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansdlima(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "d%" and jawaban=5');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansdenam(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "d%" and jawaban=6');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		//baca jawaban ST
		public function jumlahjawabanstsatu(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "t%" and jawaban=1');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabanstdua(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "t%" and jawaban=2');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansttiga(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "t%" and jawaban=3');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabanstempat(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "t%" and jawaban=4');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabanstlima(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "t%" and jawaban=5');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabanstenam(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "t%" and jawaban=6');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		//baca jawaban SO
		public function jumlahjawabansosatu(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "o%" and jawaban=1');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansodua(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "o%" and jawaban=2');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansotiga(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "o%" and jawaban=3');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansoempat(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "o%" and jawaban=4');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansolima(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "o%" and jawaban=5');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabansoenam(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "o%" and jawaban=6');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		
		//baca jawaban CSI
		public function jumlahjawabancsisatu(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "c%" and jawaban=1');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabancsidua(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "c%" and jawaban=2');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabancsitiga(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "c%" and jawaban=3');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabancsiempat(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "c%" and jawaban=4');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabancsilima(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "c%" and jawaban=5');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		public function jumlahjawabancsienam(){
			$a = $this->db->query('select *from isikuesioner where kodepertanyaan like "c%" and jawaban=6');
			$hitung_a = $a->num_rows();
			return $hitung_a;	
		}
		//baca jumlah pertanyaan keseluruhan
		public function jumlahpertanyaan(){
			$f = $this->db->query('select *from isikuesioner');
			$total = $f->num_rows();
			return $total;			
		}
		//baca pertanyaan ai to ss
		public function jumlahpertanyaanss(){
			$f = $this->db->query('select *from isikuesioner where kodepertanyaan like "s%"');
			$total = $f->num_rows();
			return $total;			
		}
		
		//baca pertanyaan ds to sd
		public function jumlahpertanyaansd(){
			$f = $this->db->query('select *from isikuesioner where kodepertanyaan like "d%"');
			$total = $f->num_rows();
			return $total;			
		}
		//baca pertanyaan ST
		public function jumlahpertanyaanst(){
			$f = $this->db->query('select *from isikuesioner where kodepertanyaan like "t%"');
			$total = $f->num_rows();
			return $total;			
		}
		//baca pertanyaan SO
		public function jumlahpertanyaanso(){
			$f = $this->db->query('select *from isikuesioner where kodepertanyaan like "o%"');
			$total = $f->num_rows();
			return $total;			
		}
		//baca pertanyaan CSI
		public function jumlahpertanyaancsi(){
			$f = $this->db->query('select *from isikuesioner where kodepertanyaan like "c%"');
			$total = $f->num_rows();
			return $total;			
		}
		public function jumlahres(){
			$t = $this->db->query('select * from pengguna where kodepengguna not like "a%"');
			$tot = $t->num_rows();
			return $tot;
		}
		//tambah data
		/*public function tambah_data($table,$data){
			$res = $this->db->insert($table,$data);
			return $res;
		}*/
	}

?>