<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_kelolapengguna extends CI_Model {

		function baca_data() {
			$t = $this->db->query('select *from pengguna where kodepengguna not like "a%"');
			return $t->result();
		}
		
		function tambah_data($table,$data){
			$res = $this->db->insert($table,$data);
			return $res;
		}
		
		function hapus_data($where,$table){
			$this->db->where($where);
			$this->db->delete($table);
		}
		
		function ubah_data($where,$table){
			return $this->db->get_where($table,$where);
		}
		
		function ubah_datapengguna($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}
		
		function jumlah(){
			$t = $this->db->query('select *from pengguna');
			$h = $t->num_rows();
			
			return $h;
		}
	}

?>