<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_kelolakuesioner extends CI_Model {

		function baca_data() {
			$data = $this->db->get('kuesioner');
			return $data->result();
		}
		
		function tambah_data($table,$data){
			$res = $this->db->insert($table,$data);
			return $res;
		}
		
		function ubah_data($where,$table){
			return $this->db->get_where($table,$where);
		}
		
		function ubah_datakuesioner($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}
		
		function hapus_data($where,$table){
			$this->db->where($where);
			$this->db->delete($table);
		}
		
	}

?>