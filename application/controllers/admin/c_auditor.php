<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_Auditor extends CI_Controller {

	public function index() {
		$this->load->model('m_rekapitulasi');
		$data['t']=$this->m_rekapitulasi->jumlahres();
		$data['r']=$this->m_rekapitulasi->baca_responden();
		$this->load->view('admin/v_auditor',$data);
	}					
}
?>