<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_kelolakuesioner extends CI_Controller {

	public function index() {		
		$this->load->model('m_kelolakuesioner');
		$data['user']= $this->m_kelolakuesioner->baca_data();
		$this->load->view('admin/v_kelolakuesioner', $data);
	}
	
	public function tambahkuesioner(){
		$this->load->view('admin/v_tambahkuesioner');
	}
	
	public function aksi_tambahkuesioner(){
		$this->load->model('m_kelolakuesioner');
		$data = array(
			'kodepertanyaan'=>$this->input->post('kodepertanyaan'),
			'pertanyaan'=>$this->input->post('pertanyaan'),
		);
		$data = $this->m_kelolakuesioner->tambah_data('kuesioner',$data);
		if($data>=1){
			echo "<script>alert('Data berhasil disimpan');</script>";
			redirect('admin/c_kelolakuesioner/','refresh');
		}else{
			echo "<script>alert('Data gagal disimpan');</script>";
			redirect('admin/c_kelolakuesioner/tambahkuesioner','refresh');
		}
	}
	
	public function ubahkuesioner($id){
		$this->load->model('m_kelolakuesioner');
		$where = array('kodepertanyaan'=>$id);
		$data['user'] = $this->m_kelolakuesioner->ubah_data($where,'kuesioner')->result();
		$this->load->view('admin/v_ubahkuesioner',$data);
	}
	
	public function aksi_ubahkuesioner(){
		$this->load->model('m_kelolakuesioner');
		$kodepertanyaan = $this->input->post('kodepertanyaan');
		$pertanyaan = $this->input->post('pertanyaan');
		
		$data = array(
			'pertanyaan' =>$pertanyaan,
		);
		
		$where = array(
			'kodepertanyaan' =>$kodepertanyaan
		);
		
		if($this->m_kelolakuesioner->ubah_datakuesioner($where,$data,'kuesioner')>=1){
			echo "<script>alert('Data gagal diubah');</script>";
			redirect('admin/c_kelolakuesioner/ubahkuesioner/','refresh');
		}else{
			echo "<script>alert('Data berhasil diubah');</script>";
			redirect('admin/c_kelolakuesioner/','refresh');
		}
	}

	public function hapuskuesioner($id){
		$this->load->model('m_kelolakuesioner');
		$where = array('kodepertanyaan'=>$id);
		$data=$this->m_kelolakuesioner->hapus_data($where,'kuesioner');
		
		if($data>=1){
		echo "<script>alert('Data gagal dihapus');</script>";
			redirect('admin/c_kelolakuesioner/','refresh');
		}else{
			echo "<script>alert('Data berhasil dihapus');</script>";
			redirect('admin/c_kelolakuesioner/','refresh');
		}
	}
}
?>