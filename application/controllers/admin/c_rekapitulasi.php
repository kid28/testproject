<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_Rekapitulasi extends CI_Controller {

	public function index() {
		//untuk rekap keseluruhan
		$this->load->model('m_rekapitulasi');
		$data['tampil']= $this->m_rekapitulasi->baca_responden();
		$data['jumlahpertanyaan'] = $this->m_rekapitulasi->jumlahpertanyaan();
		$data['tampilsatu'] = $this->m_rekapitulasi->jumlahjawabansatu();
		$data['tampildua'] = $this->m_rekapitulasi->jumlahjawabandua();
		$data['tampiltiga'] = $this->m_rekapitulasi->jumlahjawabantiga();
		$data['tampilempat'] = $this->m_rekapitulasi->jumlahjawabanempat();
		$data['tampillima'] = $this->m_rekapitulasi->jumlahjawabanlima();
		//dipindahkan hilangkan karna sampe lvl 5
		//$data['tampilenam'] = $this->m_rekapitulasi->jumlahjawabanenam();
		//untuk rekap ss
		$data['ss_tampil'] = $this->m_rekapitulasi->baca_respondenss();
		$data['ss_jumlahpertanyaan'] = $this->m_rekapitulasi->jumlahpertanyaanss();
		$data['ss_tampilsatu'] = $this->m_rekapitulasi->jumlahjawabansssatu();
		$data['ss_tampildua'] = $this->m_rekapitulasi->jumlahjawabanssdua();
		$data['ss_tampiltiga'] = $this->m_rekapitulasi->jumlahjawabansstiga();
		$data['ss_tampilempat'] = $this->m_rekapitulasi->jumlahjawabanssempat();
		$data['ss_tampillima'] = $this->m_rekapitulasi->jumlahjawabansslima();
		$data['ss_tampilenam'] = $this->m_rekapitulasi->jumlahjawabanssenam();
		//untuk rekap ds to sd
		$data['sd_tampil'] = $this->m_rekapitulasi->baca_respondensd();
		$data['sd_jumlahpertanyaan'] = $this->m_rekapitulasi->jumlahpertanyaansd();
		$data['sd_tampilsatu'] = $this->m_rekapitulasi->jumlahjawabansdsatu();
		$data['sd_tampildua'] = $this->m_rekapitulasi->jumlahjawabansddua();
		$data['sd_tampiltiga'] = $this->m_rekapitulasi->jumlahjawabansdtiga();
		$data['sd_tampilempat'] = $this->m_rekapitulasi->jumlahjawabansdempat();
		$data['sd_tampillima'] = $this->m_rekapitulasi->jumlahjawabansdlima();
		$data['sd_tampilenam'] = $this->m_rekapitulasi->jumlahjawabansdenam();
		//untuk rekap ST
		$data['st_tampil'] = $this->m_rekapitulasi->baca_respondenst();
		$data['st_jumlahpertanyaan'] = $this->m_rekapitulasi->jumlahpertanyaanst();
		$data['st_tampilsatu'] = $this->m_rekapitulasi->jumlahjawabanstsatu();
		$data['st_tampildua'] = $this->m_rekapitulasi->jumlahjawabanstdua();
		$data['st_tampiltiga'] = $this->m_rekapitulasi->jumlahjawabansttiga();
		$data['st_tampilempat'] = $this->m_rekapitulasi->jumlahjawabanstempat();
		$data['st_tampillima'] = $this->m_rekapitulasi->jumlahjawabanstlima();
		$data['st_tampilenam'] = $this->m_rekapitulasi->jumlahjawabanstenam();
		
		//untuk rekap SO
		$data['so_tampil'] = $this->m_rekapitulasi->baca_respondenso();
		$data['so_jumlahpertanyaan'] = $this->m_rekapitulasi->jumlahpertanyaanso();
		$data['so_tampilsatu'] = $this->m_rekapitulasi->jumlahjawabansosatu();
		$data['so_tampildua'] = $this->m_rekapitulasi->jumlahjawabansodua();
		$data['so_tampiltiga'] = $this->m_rekapitulasi->jumlahjawabansotiga();
		$data['so_tampilempat'] = $this->m_rekapitulasi->jumlahjawabansoempat();
		$data['so_tampillima'] = $this->m_rekapitulasi->jumlahjawabansolima();
		$data['so_tampilenam'] = $this->m_rekapitulasi->jumlahjawabansoenam();
		
		//untuk rekap CSI
		$data['csi_tampil'] = $this->m_rekapitulasi->baca_respondencsi();
		$data['csi_jumlahpertanyaan'] = $this->m_rekapitulasi->jumlahpertanyaancsi();
		$data['csi_tampilsatu'] = $this->m_rekapitulasi->jumlahjawabancsisatu();
		$data['csi_tampildua'] = $this->m_rekapitulasi->jumlahjawabancsidua();
		$data['csi_tampiltiga'] = $this->m_rekapitulasi->jumlahjawabancsitiga();
		$data['csi_tampilempat'] = $this->m_rekapitulasi->jumlahjawabancsiempat();
		$data['csi_tampillima'] = $this->m_rekapitulasi->jumlahjawabancsilima();
		$data['csi_tampilenam'] = $this->m_rekapitulasi->jumlahjawabancsienam();
		
		//$this->load->view('auditor/v_rekapitulasi',$data);
		$this->load->view('admin/v_rekapitulasi-testitil',$data);
	
	}	
	/**
	public function aksi_tambahai(){
		$this->load->model('m_rekapitulasi');
		$data = array(
			'namadomain'=>$this->input->post("namaai"),
			'nilai'=>$this->input->post("nilaimaturityai"),
			'keterangan'=>$this->input->post("keteranganai")
		);
		$datas = $this->m_rekapitulasi->tambah_data('rekapitulasi',$data);
		redirect ('auditor/c_rekapitulasi');
	}
	public function aksi_tambahds(){
		$this->load->model('m_rekapitulasi');
		$data = array(
			'namadomain'=>$this->input->post("namads"),
			'nilai'=>$this->input->post("nilaimaturityds"),
			'keterangan'=>$this->input->post("keterangands")
		);
		$datas = $this->m_rekapitulasi->tambah_data('rekapitulasi',$data);
		redirect ('auditor/c_rekapitulasi');
	}
	public function aksi_tambahme(){
		$this->load->model('m_rekapitulasi');
		$data = array(
			'namadomain'=>$this->input->post("namame"),
			'nilai'=>$this->input->post("nilaimaturityme"),
			'keterangan'=>$this->input->post("keteranganme")
		);
		$datas = $this->m_rekapitulasi->tambah_data('rekapitulasi',$data);
		redirect ('auditor/c_rekapitulasi');
	}
	*/
}
?>