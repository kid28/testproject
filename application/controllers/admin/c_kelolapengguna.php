<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_kelolapengguna extends CI_Controller {

	public function index(){
		$this->load->model('m_kelolapengguna');
		$data['user'] = $this->m_kelolapengguna->baca_data();
		$this->load->view('admin/v_kelolapengguna', $data);
	}
	
	public function tambahpengguna(){
		$this->load->view('admin/v_tambahpengguna');
	}
	
	public function aksi_tambahpengguna(){
		$this->load->model('m_kelolapengguna');
		$data = array(
			'kodepengguna'=>$this->input->post('kodepengguna'),
			'username'=>$this->input->post('username'),
			'password'=>$this->input->post('password'),
			'nama'=>$this->input->post('nama'),
			'level'=>$this->input->post('level')
		);
		$data = $this->m_kelolapengguna->tambah_data('pengguna',$data);
		if($data>=1){
			echo "<script>alert('Data berhasil disimpan');</script>";
			redirect('admin/c_kelolapengguna/','refresh');
		}else{
			echo "<script>alert('Data gagal disimpan');</script>";
			redirect('admin/c_kelolapengguna/tambahpengguna','refresh');
		}
	}
	
	public function hapuspengguna($id){
		$this->load->model('m_kelolapengguna');
		$where = array('kodepengguna'=>$id);
		$data = $this->m_kelolapengguna->hapus_data($where,'pengguna');
		if($data>=1){
		echo "<script>alert('Data gagal dihapus');</script>";
			redirect('admin/c_kelolapengguna/','refresh');
		}else{
			echo "<script>alert('Data berhasil dihapus');</script>";
			redirect('admin/c_kelolapengguna/','refresh');
		}
		
	}
	
	public function ubahpengguna($id){
		$this->load->model('m_kelolapengguna');
		$where = array('kodepengguna'=>$id);
		$data['user'] = $this->m_kelolapengguna->ubah_data($where,'pengguna')->result();
		$this->load->view('admin/v_ubahpengguna',$data);
	}
	
	public function aksi_ubahpengguna(){
		$this->load->model('m_kelolapengguna');
		$kodepengguna = $this->input->post('kodepengguna');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$nama = $this->input->post('nama');
		$level = $this->input->post('level');
		
		$data = array(
			'username' =>$username,
			'password' =>$password,
			'nama' 	   =>$nama,
			'level'    =>$level
		);
		
		$where = array(
			'kodepengguna' =>$kodepengguna
		);
		
		
		if($this->m_kelolapengguna->ubah_datapengguna($where,$data,'pengguna')>=1){
			echo "<script>alert('Data gagal diubah');</script>";
			redirect('admin/c_kelolapengguna/ubahpengguna/','refresh');
		}else{
			echo "<script>alert('Data berhasil diubah');</script>";
			redirect('admin/c_kelolapengguna/','refresh');
		}
		redirect('admin/c_kelolapengguna');
	}
}
?>