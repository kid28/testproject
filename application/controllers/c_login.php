<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_login extends CI_Controller {

	public function index() {
		$this->load->view('v_login');
	}

	public function cek_login() {
		$data = array(
					'username' => $this->input->post('username', TRUE),
					'password' => $this->input->post('password', TRUE));
		$this->load->model('m_login'); // load m_login
		$hasil = $this->m_login->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Loggin';
				$sess_data['kodepengguna'] = $sess->kodepengguna;
				$sess_data['username'] = $sess->username;
				$sess_data['level'] = $sess->level;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('level')=='AD') {
				redirect('admin/c_auditor');
			}
			elseif ($this->session->userdata('level')=='SA') {
				redirect('admin/c_admin');
			}
			elseif ($this->session->userdata('level')=='mahasiswa') {
				//redirect('responden/c_strategis');
				redirect('responden/c_mahasiswa');
			}
			elseif ($this->session->userdata('level')=='dosen') {
				//redirect('responden/c_taktis');
				redirect('responden/c_dosen');
			}
		
		}
		else {
			echo "<script>alert('Maaf anda gagal login, silahkan cek username dan password anda!')</script>";
			redirect('c_login','refresh');
		}
	}			
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('c_login','refresh');
	}
}
?>