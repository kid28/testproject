<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_isidosen extends CI_Controller {

	public function index() {
		$this->load->model('m_isikuesioner');
		$data['tampil']= $this->m_isikuesioner->baca_datadosen();
		$this->load->view('responden/v_isidosen', $data);
	}
	
	public function aksi_tambahisi(){		
		$this->load->model('m_isikuesioner');
		$i = 1;
		$a = $this->db->query('SELECT *FROM kuesioner WHERE kodepertanyaan LIKE "d%"');
		$hitung = $a->num_rows();
		
		for($i = 1; $i<=$hitung;$i++){
			$data = array(
				'kodepengguna'=>$this->session->userdata('kodepengguna'),
				'kodepertanyaan' =>$this->input->post("kodepertanyaan".$i),
				'jawaban'=> $this->input->post("jawaban".$i)
			);
			$data = $this->m_isikuesioner->tambahdata('isikuesioner',$data);
			if($data>=1){
				echo "<script>alert('Data berhasil disimpan');</script>";
				redirect('responden/c_dosen/','refresh');
			}else{
				echo "<script>alert('Data gagal disimpan');</script>";
				redirect('responden/c_isidosen/','refresh');
			}
		}
	}
}
?>