<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_isimahasiswa extends CI_Controller {

	public function index() {
		$this->load->model('m_isikuesioner');
		$data['tampil']= $this->m_isikuesioner->baca_datamahasiswa();
		$this->load->view('responden/v_isimahasiswa', $data);
	}
	
	public function aksi_tambahisi(){
		$this->load->model('m_isikuesioner');
		$i = 1;
		// testing $a = $this->db->get(kuesioner);
		$a = $this->db->query('SELECT *FROM kuesioner');
		$hitung = $a->num_rows();
		for($i = 1; $i<=$hitung;$i++){
			$data = array(
				'kodepengguna'=>$this->session->userdata['kodepengguna'],
				'kodepertanyaan'=>$this->input->post("kodepertanyaan".$i),
				'jawaban'=> $this->input->post("jawaban".$i)
			);
			$data = $this->m_isikuesioner->tambahdata('isikuesioner',$data);
			
		}
		if($data>=1){
				echo "<script>alert('Data berhasil disimpan');</script>";
				redirect('responden/c_mahasiswa/','refresh');
			}else{
				echo "<script>alert('Data gagal disimpan');</script>";
				redirect('responden/c_isimahasiswa/','refresh');
		}
	}
}
?>